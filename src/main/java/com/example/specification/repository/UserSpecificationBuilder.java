package com.example.specification.repository;

import com.example.specification.model.SearchCriteria;
import com.example.specification.model.UserTest;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class UserSpecificationBuilder {
    private final List<SearchCriteria> param = new ArrayList<>();
    public UserSpecificationBuilder(List<SearchCriteria> criteria) {
        param.addAll(criteria);
    }
    public Specification<UserTest> build() {
        if (param.isEmpty()) {return null;}
        List<UserSpecification> specs = param.stream()
                .map(UserSpecification::new)
                .collect(Collectors.toList());
        Specification<UserTest> result = specs.get(0);
        for (int i = 1; i < param.size(); i++) {
            result = Specification.where(result)
                    .and(specs.get(i));
        }
        return result;
    }
}
