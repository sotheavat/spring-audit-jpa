package com.example.specification.repository;

import com.example.specification.model.UserTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.Future;

@Repository
public interface UserRepository extends JpaRepository<UserTest,Long>, JpaSpecificationExecutor<UserTest> {
    @Async
    Future<UserTest> findByFirstName(String x);
}
