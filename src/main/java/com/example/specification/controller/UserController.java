package com.example.specification.controller;

import com.example.specification.model.SearchCriteria;
import com.example.specification.model.UserDto;
import com.example.specification.model.UserTest;
import com.example.specification.repository.UserRepository;
import com.example.specification.repository.UserSpecificationBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/")
public class UserController {
    private final UserRepository userRepository;
    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @GetMapping("/users")
    public ResponseEntity<Object> search(@RequestParam String pattern) {
        List<SearchCriteria> specList = Arrays.stream(pattern.split(",")).map(s-> new SearchCriteria(s.split(":")[0],":",s.split(":")[1])).collect(Collectors.toList());
        return new ResponseEntity<>(userRepository.findAll(new UserSpecificationBuilder(specList).build()), HttpStatus.OK);
    }

    @GetMapping("/users/{firstname}")
    public ResponseEntity<Object> searchByFirstName(@PathVariable String firstname) throws ExecutionException, InterruptedException {
        return new ResponseEntity<>(userRepository.findByFirstName(firstname).get(), HttpStatus.OK);
    }


    @PostMapping("/users")
    public String addUsers(@RequestBody UserDto userDto){
        UserTest user = new UserTest(userDto);
        userRepository.save(user);
        return "spring suck";
    }

    @PutMapping("/users/{id}")
    public String updateUser(@RequestBody UserDto userDto, @PathVariable Long id){
        UserTest user = userRepository.findById(id).get();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setAge(userDto.getAge());
        user.setEmail(userDto.getEmail());
        userRepository.save(user);
        return "spring suck";
    }
}
